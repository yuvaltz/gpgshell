@echo off
"%~dp0clipboard" get | "C:\Program Files (x86)\gnupg\bin\gpg.exe" --encrypt -a --recipient UserName -o "%*"
if /i "%errorlevel%" neq "0" pause
