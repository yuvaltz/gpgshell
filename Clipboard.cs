﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Clipboard
{
    class Program
    {
        private const int CF_TEXT = 1;
        private const int CF_UNICODETEXT = 13;

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern bool OpenClipboard(IntPtr hWndNewOwner);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr GetClipboardData(uint uFormat);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SetClipboardData(uint uFormat, IntPtr hMem);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern bool EmptyClipboard();

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern bool CloseClipboard();

        private const int MaxOpenClipboardAttempts = 10;
        private const int OpenClipboardAttemptDelay = 100;

        private enum ProcessResult { Success = 0, Failure = 1, Usage = 2}

        public static int Main(string[] args)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            Console.CancelKeyPress += (sender, e) => cancellationTokenSource.Cancel();
            Console.InputEncoding = Encoding.UTF8;
            Console.OutputEncoding = Encoding.UTF8;

            return (int)MainAsync(args, cancellationTokenSource.Token).Result;
        }

        private static async Task<ProcessResult> MainAsync(string[] args, CancellationToken cancellationToken)
        {
            try
            {
                if (args.Length >= 1 && args[0] == "get")
                {
                    await GetClipboardAsync(cancellationToken);
                    return ProcessResult.Success;
                }

                if (args.Length >= 1 && args[0] == "set")
                {
                    await SetClipboardAsync(cancellationToken);
                    return ProcessResult.Success;
                }

                if (args.Length >= 2 && args[0] == "timeout" && TryParseTimeSpan(args[1], out var timeSpan))
                {
                    await TimeoutClipboardAsync(timeSpan, cancellationToken);
                    return ProcessResult.Success;
                }

                if (args.Length >= 2 && args[0] == "timeout-process" && TryParseTimeSpan(args[1], out timeSpan))
                {
                    await TimeoutClipboardProcessAsync(timeSpan, cancellationToken);
                    return ProcessResult.Success;
                }

                if (args.Length > 0)
                {
                    Console.WriteLine("Invalid parameters");
                }

                Console.WriteLine($"Usage: {AppDomain.CurrentDomain.FriendlyName} [get|set|timeout<s|hh:mm:ss>]");
                return ProcessResult.Usage;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message}");
                return ProcessResult.Failure;
            }
        }

        private static async Task GetClipboardAsync(CancellationToken cancellationToken)
        {
            var value = await GetClipboardTextAsync(cancellationToken);
            Console.WriteLine(value);
        }

        private static async Task SetClipboardAsync(CancellationToken cancellationToken)
        {
            var value = await ReadInputAsync(cancellationToken);
            await SetClipboardTextAsync(value, cancellationToken);
        }

        private static async Task TimeoutClipboardAsync(TimeSpan timeSpan, CancellationToken cancellationToken)
        {
            await Task.Run(() => Process.Start(new ProcessStartInfo(Assembly.GetExecutingAssembly().Location, $"timeout-process {timeSpan}") { WindowStyle = ProcessWindowStyle.Hidden }));
        }

        private static async Task TimeoutClipboardProcessAsync(TimeSpan timeSpan, CancellationToken cancellationToken)
        {
            var initialValue = await GetClipboardTextAsync(cancellationToken);

            await Task.Delay(timeSpan, cancellationToken);

            var value = await GetClipboardTextAsync(cancellationToken);

            if (!cancellationToken.IsCancellationRequested && initialValue == value)
            {
                await SetClipboardTextAsync(null, cancellationToken);
            }
        }

        private static async Task<string> ReadInputAsync(CancellationToken cancellationToken)
        {
            using (var stream = Console.OpenStandardInput())
            {
                using (var streamReader = new StreamReader(stream, Encoding.UTF8))
                {
                    if (Console.IsInputRedirected)
                    {
                        var value = await streamReader.ReadToEndAsync();

                        if (String.IsNullOrEmpty(value))
                        {
                            throw new Exception("Empty input stream");
                        }

                        return value.EndsWith(Environment.NewLine) ? value.Substring(0, value.Length - Environment.NewLine.Length) : value;
                    }

                    string line;
                    var lines = new List<string>();

                    while (!cancellationToken.IsCancellationRequested && !String.IsNullOrEmpty(line = await streamReader.ReadLineAsync()))
                    {
                        lines.Add(line);
                    }

                    return String.Join(Environment.NewLine, lines);
                }
            }
        }

        private static async Task<string> GetClipboardTextAsync(CancellationToken cancellationToken)
        {
            var attempts = MaxOpenClipboardAttempts;
            while (attempts > 0 && !cancellationToken.IsCancellationRequested)
            {
                if (OpenClipboard(IntPtr.Zero))
                {
                    try
                    {
                        return Marshal.PtrToStringUni(GetClipboardData(CF_UNICODETEXT));
                    }
                    finally
                    {
                        CloseClipboard();
                    }
                }

                attempts--;

                if (attempts > 0)
                {
                    await Task.Delay(OpenClipboardAttemptDelay, cancellationToken);
                }
            }

            throw new Exception("Failed to open clipboard");
        }

        private static async Task SetClipboardTextAsync(string value, CancellationToken cancellationToken)
        {
            var attempts = MaxOpenClipboardAttempts;
            var valuePtr = Marshal.StringToHGlobalUni(value);

            while (attempts > 0 && !cancellationToken.IsCancellationRequested)
            {
                if (OpenClipboard(IntPtr.Zero))
                {
                    try
                    {
                        if (EmptyClipboard() && SetClipboardData(CF_UNICODETEXT, valuePtr) == valuePtr)
                        {
                            return;
                        }
                    }
                    finally
                    {
                        CloseClipboard();
                    }
                }

                attempts--;

                if (attempts > 0)
                {
                    await Task.Delay(OpenClipboardAttemptDelay, cancellationToken);
                }
            }

            Marshal.FreeHGlobal(valuePtr);
            throw new Exception("Failed to open clipboard");
        }

        private static bool TryParseTimeSpan(string value, out TimeSpan result)
        {
            if (Int32.TryParse(value, out var seconds))
            {
                result = TimeSpan.FromSeconds(seconds);
                return true;
            }

            return TimeSpan.TryParse(value, out result);
        }
    }
}
