Windows context menu for GnuPG
------------------------------
![screenshot](./screenshot.png)

Installation
------------
1. Download and install **GnuPG 2.2.*** from https://gnupg.org
(click on the download link where it says *"Simple installer for the current GnuPG"*, **do not** download Gpg4win)
2. Generate a new private key with `gpg --full-generate-key`
3. Review and then build ***Clipboard.cs*** with
`csc -out:clipboard.exe Clipboard.cs`
(or `"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\Roslyn\csc.exe" -out:clipboard.exe Clipboard.cs`)
4. Set your recipient/user name in ***shell-overwrite.cmd***
5. Copy ***clipboard.exe***, and ****.cmd*** files to a permanent folder
6. Replace **"C:\\\\GpgShell"** with the permanent folder path in ***Registry.reg*** and apply it
7. *(optional)* Create a shortcut to ***copy-random.cmd*** to generate random passwords (edit it to change the passwords length)

Usage
-----
1. *(optional)* Create a git repository to manage your passwords
2. Encrypt and save a password:
    1. Copy the password to the clipboard
    2. Create an empty file with a ***.gpg*** extension
    (an empty ***"Gpg file"*** template will be added to folder's "New" context menu after logout)
    3. **Right click** on the file and select ***"Overwrite with clipboard content"***
3. Decrypt and copy a password to clipboard:
    1. **Double click** on a ***.gpg*** file (or **Right click** and select ***"Copy to clipboard"***)
    2. The clipboard content will be automatically cleared after 60 seconds (defined in ***shell-copy.cmd***)

Useful gpg commands
-------------------
* Create a new key
`gpg --full-generate-key`
* Encrypt
    * `gpg --encrypt -a --recipient UserName -o message.txt`
    * `echo "message" | gpg --encrypt -a --recipient UserName > message.txt`
* Decrypt
    * `gpg --decrypt -a -i message.txt`
    * `type message.txt | gpg --decrypt -a`
* List keys
`gpg --list-keys`
`gpg --list-secret-keys`
* Export secret key
`gpg -a --export-secret-keys UserName > UserName-secret-key.gpg`
* Import secret key
`gpg --import UserName-secret-key.gpg`
* Delete secret key
`gpg --delete-secret-key UserName`
`gpg --delete-key UserName` (after secret key is deleted)
(keys are stored at `%USERPROFILE%\AppData\Roaming\gnupg`)
* Edit secret key
`gpg --edit-key UserName`
    * `passwd` (change password)
    * `trust` (set trust level, usually for imported private keys)
    * `save`
* Restart agent to clear the temporarily stored passwords in memory
`gpgconf --kill gpg-agent`
