@echo off
echo "C:\Program Files (x86)\gnupg\bin\gpg.exe" --decrypt -a "%*"
"C:\Program Files (x86)\gnupg\bin\gpg.exe" --decrypt -a "%*" | "%~dp0clipboard" set
if /i "%errorlevel%" equ "0" ("%~dp0clipboard" timeout 60 & echo Success & timeout /t 1 > nul) else (echo Failure & pause)
